package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"gitlab.com/fadil.xcoder/go-webapp-db-docker/types"
)

func main() {
	godotenv.Load(".env")

	router := mux.NewRouter()

	router.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		var users []types.User
		var userDetails []map[string]interface{}
		var user types.User // base on types/user.go

		userId := os.Getenv("USER_ID")

		connStr := "postgresql://admcfoiw:cEm9H-wCgjS2g0BUGmpxuE7LkQh7J4yk@rosie.db.elephantsql.com/admcfoiw"

		// Connect to database
		db, err := sql.Open("postgres", connStr)
		if err != nil {
			log.Fatal(err)
		}

		rows, err := db.Query("SELECT id, idx, ref_number, image_blob FROM public.users_list")

		defer rows.Close()

		if err != nil {
			log.Fatalln(err)
		}

		for rows.Next() {
			if err := rows.Scan(&user.Id, &user.Data, &user.RefNumber, &user.ImageBlob); err != nil {
				log.Fatalln(err)
			}
			users = append(users, user)
		}

		// Check for errors from iterating over rows.
		if err := rows.Err(); err != nil {
			log.Fatalln(err)
		}

		for _, user := range users {
			userDetail := map[string]interface{}{
				"id":   user.Id,
				"data": user.Data,
				"ref":  user.RefNumber,
			}
			userDetails = append(userDetails, userDetail)
		}

		response := map[string]interface{}{
			"user_id": userId,
			"payload": userDetails,
		}

		rw.Header().Set("Content-Type", "application/json")
		json.NewEncoder(rw).Encode(response)
	})

	log.Println("Server is running!")
	fmt.Println(http.ListenAndServe(":8581", router)) // app port inside docker
}
