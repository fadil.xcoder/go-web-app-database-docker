
# Dockerize Go App with PostgreSQL


Using third party database for posgresql (https://www.elephantsql.com/)

App : `http://localhost:8885/`

Json response

```json
{
  "payload": [
    {
      "data": "Yoooo FX4",
      "id": 5,
      "ref": 726949478
    },
    {
      "data": "Yoooo FX5",
      "id": 6,
      "ref": 987564327
    }
  ],
  "user_id": "abxdf876POI6tst4tsbbt2f333taHUYs18JF412fsdpuXXt5c81fadil@xcoder.dvlpr"
}
```

```go
db.Query("SELECT id, idx, ref_number, image_blob FROM public.users_list")

db.Query("SELECT id, data, ref_number, image_blob FROM public.users_list")
```

Fields mapping is based on ordering

```go
rows.Scan(&user.Id, &user.Data, &user.RefNumber, &user.ImageBlob);
```