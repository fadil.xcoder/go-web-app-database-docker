package types

type User struct {
	Id        int
	Data      string
	RefNumber int
	ImageBlob string
}
