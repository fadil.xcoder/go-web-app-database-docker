FROM golang:alpine3.19 as build-env

ENV APP_NAME web-app-db-docker
ENV CMD_PATH main.go
ENV GO111MODULE=on
# possible values are windows, darwin, js
ENV GOOS linux

RUN mkdir -p /go/src/$APP_NAME

COPY . /go/src/$APP_NAME
WORKDIR /go/src/$APP_NAME

RUN go install github.com/cespare/reflex@latest

EXPOSE 8885

CMD reflex -g '*.go' go run ./$CMD_PATH --start-service
